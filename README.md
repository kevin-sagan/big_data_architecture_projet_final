# Projet Big Data Architecture

[[_TOC_]]


#### Membres de l'équipe

* Marie MENDY
* Kevin SAGAN
* Fadi EL CHEIKH TAHA
* Julien RIBEIRO
* Celia GUYOBON
* Roumaissa OMARI

## Première partie
#### Exploiter la data de la SNCF

Dans le cadre d'un projet scolaire, nous avons utilisé le langage PySpark pour exploiter la donnée de la SNCF.
Nous avons donc récupéré le [dataset des tarifs de TGV](https://ressources.data.sncf.com/explore/dataset/tarifs-tgv-par-od/export/).
Nous souhaitions spliter les gares de la colonne OD (Origine - Destination) en deux.

```
+--------------------+------------------+-----------------------+-----------+------------+
|                  OD|Prix d'appel 2nde |Plein Tarif Loisir 2nde|1ère classe|Commentaires|
+--------------------+------------------+-----------------------+-----------+------------+
|  NIMES-LILLE EUROPE|              30.0|                  144.0|      184.0|        null|
|MOUTIERS SALINS B...|              30.0|                  147.0|      190.0|        null|
|      LANDRY-QUIMPER|              30.0|                  147.0|      190.0|        null|
|TGV HAUTE PICARDI...|              30.0|                  150.0|      188.0|        null|
|TGV HAUTE PICARDI...|              30.0|                  150.0|      188.0|        null|
|PARIS GARE DE LYO...|              25.0|                  121.0|      166.0|        null|
|PARIS GARE DE LYO...|              25.0|                   88.0|      119.0|        null|
|LIMOGES BENEDICTI...|              25.0|                   88.0|      111.0|        null|
+--------------------+------------------+-----------------------+-----------+------------+
```

Premier problème rencontré, certaines gares possèdent plusieurs tirets, ce qui nous pousserait à croire que le trajet se compose de 3 gares.
Exemple ci-dessous :

```
+--------------------+--------------------+---------------+------+
|            Station1|            Station2|       Station3|Price1|
+--------------------+--------------------+---------------+------+
|             BELFORT|     MONTBELIARD TGV|LONS LE SAUNIER|  53.0|
+--------------------+--------------------+---------------+------+
```

Ceux-ci ne représentaient qu'une minorité du dataset, nous avons donc décidé de drop ces données.

```
+--------------------+--------------------+-----------+
|            Station1|            Station2|1ère classe|
+--------------------+--------------------+-----------+
|               NIMES|        LILLE EUROPE|      184.0|
|MOUTIERS SALINS B...|             QUIMPER|      190.0|
|              LANDRY|             QUIMPER|      190.0|
|AEROPORT CDG 2 TG...|         ALBERTVILLE|      154.0|
|          PARIS NORD|         ALBERTVILLE|      154.0|
+--------------------+--------------------+-----------+
```

Une fois cette colonne nettoyée, nous avons estimé le prix de deux stations en passant par une fonction itineraryPrice(station1, station2)

```python
def itineraryPrice(st1, st2):
    results = spark.sql(f"SELECT Price1 FROM df_stations WHERE Station1 = '{st1}' AND Station2 = '{st2}'")
    collected=results
    if collected.count()>0:
      collected=results.collect()
      return (collected[0]["Price1"])
    else:
      return None
```
Cette fonction nous retourne le prix de l'itinéraire si ce dernier existe :

```python
itineraryPrice("NIMES", "LILLE EUROPE")
>>> '184.0'
```

Autrement, si l'itinéraire n'existe pas, la fonction ne nous renvoie rien :

```python
itineraryPrice("NIMESSS", "LILLE EUROPE")
>>> 
```

À l'aide du module python Geopy, nous avons établi un classement descendant des distances pour chaque trajet possible.

La fonction get_coordinates récupère les coordonnées de latitude et de longitude de la station souhaitée.
Si la station souhaitée n'est pas trouvée, la fonction nous retournera 0, sinon, elle inscrira les coordonnées dans le fichier "cache.p".

La deuxième fonction get_distance, nous permettra de récupérer la distance en km des deux stations indiquées.

```python
def get_coordinates(station):
    if station not in cache:
        coords = geolocator.geocode(station)
        if coords == None :
          cache[station] = 0
        else :
          cache[station] = (coords.latitude, coords.longitude)  
    return cache[station]

def get_distance(key, location1, location2):
    if key not in cache:
        distance = geodesic(location1, location2).km
        cache[key] = distance
    return cache[key]

cache_path = 'cache.p'

cache = dict()
if Path(cache_path).exists():
    cache = pickle.load(open(cache_path, 'rb'))
```

Une fois les distances récupérées, on trie ces dernières selon la distance et on affiche seulement les 10 plus grandes distances.

```python
top10distance = spark.sql("SELECT * FROM all_distance ORDER BY Distance DESC LIMIT 10")
top10distance.show()
```
```
+--------------------+------------------+----+
|              Trajet|          Distance|Prix|
+--------------------+------------------+----+
|VILLENEUVE LES AV...|12561.503190931246|12.4|
|     TOULON - ORANGE| 9760.727032347975|54.0|
|    MIRAMAS - ORANGE| 9671.941837997054|30.0|
|    VALENCE - ORANGE| 9666.496907902658|30.0|
|      ARLES - ORANGE| 9643.411814320272|21.0|
|AVIGNON SUD - ORANGE| 9637.398258949728|12.6|
|   ST PERAY - ORANGE| 9554.874440117874|30.0|
|LYON PART DIEU - ...| 9491.217962265158|58.0|
|LYON PERRACHE - O...| 9490.286931306186|58.0|
|MACON LOCHE TGV -...| 9445.444620837034|67.0|
+--------------------+------------------+----+
```

Finalement, nous souhaitions visualiser la corrélation entre le prix et la distance pour chaque trajet possible. 

```python
distance = df3.select("Distance").rdd.flatMap(lambda x: x).collect()
prix = df3.select("Prix").rdd.flatMap(lambda x: x).collect()

fig = plt.figure(figsize=(10, 6))
sns.set_style('whitegrid')

sns.lineplot(x=prix,
            y=distance)

plt.show()
plt.clf()
```

[![Corrélation entre le prix et la distance pour chaque trajet](https://i.postimg.cc/7Z6Gwhdt/t-l-chargement.png)](https://postimg.cc/WhBb7N20)

## Deuxième partie
#### Effectuer une analyse exploratoire libre

Pour cette partie d'analyse exploratoire libre, nous avons choisi les deux datasets suivants :

- [World Happiness Dataset](https://www.kaggle.com/unsdsn/world-happiness)
- [Suicide Rate Dataset](https://www.kaggle.com/andrewmvd/suicide-dataset)

Dans un premier temps, nous devions trouver un lien entre les deux datasets.
Le Suicide Rate Dataset étant moins complet, nous souhaitions voir sur quelles années nous pouvions nous concentrer.

```python
dfS.groupBy("Year").count().show()
```

```python
+----+-----+
|Year|count|
+----+-----+
|2015|  551|
|2013|  161|
|2014|  186|
|2016|  552|
|2000|  549|
|2010|  549|
|2017|  169|
+----+-----+
```

Nous avons choisi l'année la plus récente et qui avait suffisamment de données pour être pertinente.
Cette analyse se portera donc sur l'année 2016.

#### Suicide Rate Dataset

Pour explorer le dataset portant sur le suicide, nous avons affiché les 10 pays ayant le taux de suicide le plus élevé

```python
top10Suicide = spark.sql("SELECT * FROM data16T ORDER BY Suicide_Rate DESC LIMIT 10")
top10Suicide.show(truncate=True)
```

[![10 Pays au taux de suicide le plus élevé](https://i.postimg.cc/QtpwCXwv/Screenshot-2.png)](https://postimg.cc/0MycHq8C)

Nous avons également visualisé le taux de suicide en France selon les sexes.

[![Taux de suicide en France selon le sexe](https://i.postimg.cc/R0mc941W/Screenshot-1.png)](https://postimg.cc/9Dx4Y3n2)

#### World Happiness Dataset

Nous avons pu visualiser les pays les plus heureux, ainsi que les moins heureux.

```python
labels = unhappy_countries.Region.value_counts().index
colors = ['lightblue','purple','pink','lightyellow','lightgreen','grey']
sizes = unhappy_countries.Region.value_counts().values

plt.figure(figsize = (10,10))
plt.pie(sizes, labels=labels, colors=colors, autopct='%1.1f%%')
plt.title('Distribution of the Least Happy Countries by Region',color = 'Black',fontsize = 15)
```

[![Pays les moins heureux](https://i.postimg.cc/sDFPKYBP/Screenshot-3.png)](https://postimg.cc/XX8yYykX)

D'autres visualisations nous ont permis de visualiser les corrélations. C'est le cas du joint plot, qui nous met en évidence la forte corrélation du PIB sur le Happiness Rank.

[![Joint Plot entre le Rank et Economy](https://i.postimg.cc/sxnrfjZs/Screenshot-4.png)](https://postimg.cc/WqJKXPnH)

#### Corrélation entre les deux dataset

Une fois les deux datasets liés, nous avons pu mettre en avant le rapport entre les deux datasets.

```python
joinData = dfH.join(data16, dfH.Country == data16.Country,how='left')
```

Une heatmap nous permet de voir la corrélation entre les différentes données du dataset.

[![Heatmap both datasets](https://i.postimg.cc/qvs12mjC/Screenshot-5.png)](https://postimg.cc/75L1kNvx)

Nous avons pu visualiser les régions ayant le taux de suicide le plus élevé.

[![SuicidePerRegion](https://i.postimg.cc/gkJwVrsy/Screenshot-6.png)](https://postimg.cc/8s2PDpqs)

Finalement, nous avons fait un joint plot sur le dataset lié pour voir plus précisément la corrélation entre les différentes colonnes.

[![Screenshot-7.png](https://i.postimg.cc/x8dd3Fvj/Screenshot-7.png)](https://postimg.cc/8JqD14Xx)


### Conclusion

Ce projet nous a permis de mieux manipuler le langage PySpark. Nous avons pu voir comment traiter différents datasets ensemble ainsi que de trouver des corrélations entre les deux.
Finalement, nous avons aussi revu le SQL pour faire différents filtres et requêtes, ainsi que Seaborn et Matplotlib pour la visualisation.
Différentes fonctions ont été crées ce ainsi que nous avions pu découvrir le module geopy qui étaient très enrichissant pour nos compétences de programmation.
